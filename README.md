# FTDI_simple_alarm

Use a USB-Serial Converter like the FT232RL for activating a buzzer. Simple and cheap (~1,75€)

## Features:
* works on GNU/Linux only
* no drivers or other software/libs are required
* triggerd via bash or service daemon

## Limitations:
* works safe on system with only one `/dev/ttyUSB` device
* no other task can use the FTDI device for other things

## Hardware:
1. FT232RL Breakout board ~1,5€
2. NPN Transistor like 2N2222A ~0,0€
3. Active Buzzer 5V ~0,25€

### Schematics:
![](https://git.mosad.xyz/localhorst/FTDI_simple_alarm/raw/commit/13a1200aea0d3dfb92495ae7d7f4993ddc4338f6/img/schematics.png)

### FTDI EEPROM
To invert the TXD pin on the FTDI device, programming the EEPROM is needed. With the Windoofs tool "FTDI_Prog" the Template `ft232rl_alert.xml` is uplouded. For GNU/Linux is [ftx-prog](https://github.com/richardeoin/ftx-prog) available. The only difference to default is the inverted TXD pin.

## Software:

### Commmon Distributions

Make sure the device is after plug in recognized as ttyUSB with `dmesg`.<br/>
Edit the alert.sh with your device name, like `/dev/ttyUSB0`. <br/>
Run `bash ./alert.sh` for starting the alarm.<br/>

### openWrt

`opkg update`<br/>
`opkg install usbutils`<br/>
`opkg install kmod-usb-serial`<br/>
`opkg install kmod-usb-serial-ftdi`<br/>

Make shure the device is recocntied as ttyUSB after plug in with `dmesg`.<br/>
Edit the alarm.sh with your device name, like `/dev/ttyUSB0`. <br/>
Run `bash ./alert.sh` for starting the alarm.<br/>

### Service
`nano /etc/init.d/alarm`

```
#!/bin/sh /etc/rc.common

START=99

restart() {
bash /root/alert.sh > /dev/null 2>&1 &
bash /root/alert.sh start > /dev/null 2>&1 &    
}

start() {
bash /root/alert.sh > /dev/null 2>&1 &
bash /root/alert.sh start > /dev/null 2>&1 &
}

stop() {
bash /root/alert.sh > /dev/null 2>&1 &
}
```
`chmod +x /etc/init.d/alarm`

`/etc/init.d/alarm start`

`/etc/init.d/alarm stop`